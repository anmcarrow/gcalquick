My own primitive wrapper script for gcalcli (https://github.com/insanum/gcalcli), 
for better and faster create quick reminders. Feel free to use and modify it. 

Requirements: `gcalcli` tool and bash  
Syntax: Just say `gcalquick.sh`