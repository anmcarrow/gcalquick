#!/bin/bash

read -p "When (DD/MM/YYYY hh:mm): " WHEN
read -p "Subj: " TITLE

gcalcli --calendar=anmcarrow@gmail.com add \
--when="${WHEN}" \
--title="${TITLE}" \
--description='' --reminder='30' \
--where='Local' --duration='30'

exit 0
